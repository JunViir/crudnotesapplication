<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" lang="en">
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Menu</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"/>
</head>
<body>

    <!-- Menu -->

  	<div class="container">
	    <c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2 style="color:DodgerBlue;">Welcome ${pageContext.request.userPrincipal.name}</h2>
	    </c:if>
  	</div>
  	<form id="logoutForm" method="POST" action="${contextPath}/logout">
	            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
    <ul>
	    <li><a class="active" href="${contextPath}/">Home</a></li>
	    <li><a class="active" onclick="document.forms['logoutForm'].submit()">Logout</a></li>
	</ul>

	<!-- Table -->

    <div align="center">
        <h1 style="color:Tomato;">All notes</h1> <br/>

        <table border="2" width="70%" cellpadding="2">
            <tr><th>Title</th><th>Content</th><th>Updated on</th><th>Comments</th><th>Edit</th><th>Delete</th></tr>
            <c:forEach var="note" items="${list}">
                <div id="popup2?id=${note.id}" class="overlay">
                    <div class="popup">
                        <a class="close" href="">&times;</a>
                        <div class="content" align="center">
                            <h1 style="color:Tomato;">Set the note</h1>
                                <form name="SetForm" method="post" action="setnotes?id=${note.id}">

                                    Title: ${note.title} <br/>
                                    Content: <input type="text" name="content" class="form-control" placeholder="${note.content}" autofocus="true" required pattern="\S+.*"/> <br/>

                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                    <p align="center">
                                        <input type="submit" value="Set"/>
                                        <input type="reset" value="Reset">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <tr>
                    <td>${note.title}</td>
                    <td>${note.content}</td>
                    <td>${note.updatedAt}</td>
                    <td><a href="${contextPath}/comments?id=${note.id}">Comments</a></td>
                    <td>
                        <a href="#popup2?id=${note.id}"><i class="fas fa-pencil-alt" style="font-size:24px"></i></a>
                    </td>
                    <td><a href="${contextPath}/deletenotes?id=${note.id}"><i class="fa fa-trash" style="font-size:24px"></i></a></td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <div class="box" style="float: right;">
            <a class="button" id="button" href="#popup1">Add</a>
        </div>

        <!-- Add Button -->

        <div id="popup1" class="overlay">
            <div class="popup">
                <a class="close" href="">&times;</a>
            	    <div class="content">
            		    <h2 style="color:Tomato;">Add a note</h2>
                         <form name="AddForm" modelAttribute="noteForm" method="post" action="addnotes">

                            Title: <input type="text" class="form-control" name="title" autofocus="true" required pattern="\S+.*"/> <br/>
                            Content: <input type="text" class="form-control" name="content" required pattern="\S+.*"/> <br/>

                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                            <p>
                                <input type="submit" value="Add"/>
                                <input type="reset" value="Reset">
                            </p>
                         </form>
                    </div>
                </div>
            </div>
        </div>

	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  	    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
  	    <script src="${contextPath}/resources/js/custom.js"></script>
    </body>
</html>



            <!--
            <h2 style="color:MediumSeaGreen;"><i class="fa fa-search"></i> Find a note</h2>
            <form name="GetForm" method="post" action="getnotes">
                Id: <input type="text" name="id"/> <br/>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="submit" value="Find"/>
            </form>
            -->
