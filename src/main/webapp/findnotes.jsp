<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" lang="en">
<head>
    <meta charset="utf-8">
	<title>All notes site</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/styles.css" rel="stylesheet">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"/>
</head>
<body>

	<form id="logoutForm" method="POST" action="${contextPath}/logout">
	            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	<ul>
        <li><a class="active" href="${contextPath}/">Home</a></li>
        <li><a class="active" href="${contextPath}/addnotes">Add a note</a></li>
        <li> <a class="active" href="${contextPath}/getnotes">Get all notes</a></li>
        <li><a class="active" onclick="document.forms['logoutForm'].submit()">Logout</a></li>
    </ul>
    <div align="center">
        <h1 style="color:Tomato;">Found note</h1>

        <h2 style="color:MediumSeaGreen;"><i class="fa fa-search"></i> Find a note</h2>
        <form name="GetForm" method="post" action="findnotes">
            Id: <input type="text" name="id"/> <br/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="submit" value="Find"/>
        </form>

        <br/><br/>
		<table border="2" width="70%" cellpadding="2">
            <tr><th>Id</th><th>Title</th><th>Content</th><th>Edit</th><th>Delete</th></tr>
               <tr>
                   <td>${note.id}</td>
                   <td>${note.title}</td>
                   <td>${note.content}</td>
                   <td><a href="${contextPath}/setnotes?id=${note.id}">Edit</a></td>
                   <td><a href="${contextPath}/deletenotes?id=${note.id}">Delete</a></td>
               </tr>
       </table>
   	</div>

</body>
</html>