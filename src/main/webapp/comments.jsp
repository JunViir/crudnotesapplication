<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" lang="en">
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Comments</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"/>
</head>
<body>

    <!-- Menu -->

    <form id="logoutForm" method="POST" action="${contextPath}/logout">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
    <ul>
        <li><a class="active" href="${contextPath}/">Home</a></li>
	    <li><a class="active" onclick="document.forms['logoutForm'].submit()">Logout</a></li>
	</ul>

    <div align="center">
        <h2>Note: ${note.title}</h2>
        <h2>Content: ${note.content}</h2>
        <h2>Updated at: ${note.updatedAt}</h2>
    </div>

	<!-- Table -->

	<div align="center">

        <h1 style="color:Tomato;">Comments</h1> <br/>

        <table border="2" width="70%" cellpadding="2">
            <tr><th>Comment</th><th>Created on</th><th>Delete</th></tr>
            <c:forEach var="comment" items="${list}">
                <tr>
                    <td>${comment.content}</td>
                    <td>${comment.updatedAt}</td>
                    <td><a href="${contextPath}/deletecomment?id=${note.id}&comm=${comment.id}"><i class="fa fa-trash" style="font-size:24px"></i></a></td>
                </tr>
            </c:forEach>
       </table>
       <br/>

        <div>
            <div class="box" style="float: left;">
                <a class="button" href="${contextPath}/">Back</a>
            </div>
            <div class="box" style="float: right;">
                <a class="button" href="#popup1">Add</a>
            </div>
        </div>

        <div id="popup1" class="overlay">
            <div class="popup">
                <a class="close" href="">&times;</a>
                <div class="content">
                    <h2 style="color:Tomato;">Add a note</h2>
                        <form name="AddForm" method="post" action="addcomment?id=${note.id}">

                            Comment: <input type="text" class="form-control" name="content" autofocus="true" required pattern="\S+.*"/> <br/>

                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                            <p>
                                <input type="submit" value="Add"/>
                                <input type="reset" value="Reset">
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
    </body>
</html>