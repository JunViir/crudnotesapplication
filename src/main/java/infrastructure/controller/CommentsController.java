package infrastructure.controller;

import domain.model.entities.Comments;
import domain.model.entities.Note;
import domain.model.repositories.CommentsRepository;
import domain.model.repositories.NoteRepository;
import infrastructure.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Controller
public class CommentsController {

    @Autowired
    CommentsRepository commentsRepository;

    @Autowired
    NoteRepository noteRepository;

    //Get all comments

    @GetMapping("/comments")
    public String getComments(Model model, @RequestParam("id") Long id) {
        Note note = noteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
        List<Comments> comments = commentsRepository.findAll();
        List<Comments> list = note.getComments();
        list.sort(Comparator.comparing(Comments::getUpdatedAt));

        model.addAttribute("list", list);
        model.addAttribute("note", note);

        return "comments";
    }

    // Create a new Comment

    @PostMapping("/addcomment")
    public void addComment(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Long id) throws Exception {
        Note note = noteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
        Comments comment = new Comments();
        Date date = new Date(System.currentTimeMillis());

        comment.setUpdatedAt(date);
        comment.setContent(request.getParameter("content"));

        List<Comments> newComments = note.getComments();
        newComments.add(comment);
        note.setComments(newComments);

        commentsRepository.save(comment);
        noteRepository.save(note);

        response.sendRedirect("/comments?id=" + id);
    }


    // Delete a Comment

    @GetMapping("/deletecomment")
    public void deleteComment(HttpServletResponse response, @RequestParam(value = "id") Long id, @RequestParam(value = "comm") Long id2) throws Exception {

        Note note = noteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
        Comments comment = commentsRepository.findById(id2).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id2));

        List<Comments> newComments = note.getComments();
        newComments.remove(comment);
        note.setComments(newComments);

        commentsRepository.delete(comment);
        noteRepository.save(note);

        response.sendRedirect("/comments?id=" + id);
    }
}