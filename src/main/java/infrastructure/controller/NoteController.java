package infrastructure.controller;

import application.validator.NoteValidator;
import domain.model.entities.Note;
import domain.model.repositories.NoteRepository;
import infrastructure.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Controller
public class NoteController {

    @Autowired
    NoteRepository noteRepository;

    @Autowired
    NoteValidator noteValidator;

    // Create a new Note

    @PostMapping("/addnotes")
    public void addNote(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("noteForm") Note note, BindingResult bindingResult) throws Exception {

        Date date = new Date(System.currentTimeMillis());

        note.setUpdatedAt(date);

        noteValidator.validate(note, bindingResult);

        if (bindingResult.hasErrors()) {
            response.sendRedirect("/#popup1");
            return;
        }

        noteRepository.save(note);

        response.sendRedirect("");
    }

    // Update a Note

    @PostMapping("/setnotes")
    public void setPost(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "id") Long id) throws Exception {

        Note note = noteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
        Date date = new Date(System.currentTimeMillis());

        note.setUpdatedAt(date);
        note.setContent(request.getParameter("content"));
        noteRepository.save(note);

        response.sendRedirect("");
    }

    // Delete a Note

    @GetMapping("/deletenotes")
    public void DeleteNote(HttpServletResponse response, @RequestParam(value = "id") Long id) throws Exception {

        Note note = noteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
        noteRepository.delete(note);

        response.sendRedirect("");
    }
    // Get a Single Note

//    @GetMapping("/findnotes")
//    public String getANote(Model model, HttpServletRequest request, @RequestParam(value = "id") Long id) {
//        Note note = noteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Note", "id", id));
//        model.addAttribute("note", note);
//        return "findnotes";
//    }
//
//    @PostMapping("/getnotes")
//    public void getPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String id = request.getParameter("id");
//        response.sendRedirect("/findnotes?id="+id);
//    }
}
