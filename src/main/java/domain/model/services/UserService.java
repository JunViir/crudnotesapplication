package domain.model.services;

import domain.model.entities.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}