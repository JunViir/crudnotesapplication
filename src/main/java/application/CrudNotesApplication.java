package application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"application", "domain.model.services", "infrastructure"})
@EntityScan("domain.model.entities")
@EnableJpaRepositories("domain.model.repositories")
public class CrudNotesApplication extends SpringBootServletInitializer {

    @Override
    public SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CrudNotesApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CrudNotesApplication.class, args);
    }


}